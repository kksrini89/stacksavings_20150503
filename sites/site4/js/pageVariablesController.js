var myapp = angular.module('mainApp', []);

myapp.controller('pagevariables', function($scope, $rootScope, $http) {

    Parse.initialize(parseAPPID, parseJSID);

    var PageVariables = Parse.Object.extend("PageVariables");
    var query = new Parse.Query(PageVariables);
    query.equalTo("pagename", page_name);

    query.find({
        success: function(results) {

            for (var i = 0; i < results.length; i++) {

                var object = results[i];

                //convert the object to JSON so we can loop through
                var jsonResult = object.toJSON()

                //loop through and set values on Angular $rootScope object for use within page
                for (var key in jsonResult) {
                    if (jsonResult.hasOwnProperty(key)) {
                        $rootScope.$apply(function() {

                            var angular_var = jsonResult[key]

                            var match = "menu_url"
                            if (key.substring(0, match.length) == match) {
                                angular_var = jsonResult["test_path"] + angular_var + "&mode=test"
                            }
                            $rootScope[key] = angular_var

                        })

                    }
                }

                break
            }
        },
        error: function(error) {
            console.log(error)
        }
    });


});